## Table of Contents

1. Objective
2. Requirements
3. Usage
4. Task list
5. Build to deploy
6. Angular Architecture
7. Sass Architecture

## Objective
The objective of this challenge is not necessarily just to solve the problem - but to evaluate your software development skills, code quality, creativity, and resourcefulness as a potential future colleague. Please share the necessary artifacts you would provide to your colleagues in a real-world professional setting.

## Requirements
1. [Node.js](https://nodejs.org/) version 12.16.2
2. [npm](https://www.npmjs.com/) - normally comes with Node.js
3. [Angular CLI](https://github.com/angular/angular-cli) version 9.1.11
4. [Karma](https://karma-runner.github.io) 
5. [Protractor](http://www.protractortest.org/)

## Usage

### Commands

```bash
ng test              # Run Karma unit test
npm run build-prod   # Run the compiler for production
npm run i18n         # Run the extraction text for localization
ng serve             # Run the local server in develop mode
```


## Task list
1. Angular multi-module application (Done).
2. Sass Folder Structure (Done). 
3. Basic i18n for localization (Done).
4. Basic unit testing (Done).
5. Develop reusable components (Done).
6. Layout look & feel (Done).
7. Implement Preloader (Done).
8. Implement spinner inside table (Done).
9. Testing e2e (To Do).
10. Performance issues (To Do).
11. Accessibility issues (To Do).
12. Error logging tracked by Slack or GA (To Do).
13. Mobile friendly design (To Do).

## Build to deploy (CI/CD)

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.


## Angular Architecture

```bash
src/
|
|– app/
|   |– config/                 # Config files
|   |– core/                   # Core module 
|   |   |– authentication/     # Authentication layer
|   |   |– guards/             # Custom guards
|   |   |– http/               # Http files
|   |   |– interceptors/       # Http interceptors
|   |   |– layout/             # Layouts
|   |   |– mocks/              # Mocks
|   |   |– models/             # Models for this module
|   |   |– services/           # Services
|   |    – core.module         # Core module configuration
|
|   |– modules/home/           # Multiples modules
|   |   |– components/         # Components for this module
|   |   |– models/             # Models for this module
|   |   |– pages/              # Pages for this module
|   |   |   |– home/           # Home Page
|
|   |– shared/                 # Shared module cross project
|   |   |– classes/            # Shared classes
|   |   |– components/         # Custom reusables components
|   |   |– directives/         # Shared Directives
|   |   |– models/             # Models reusables everywhere
|   |   |– pipes/              # Custom pipes
|   |   |– utils/              # Custom agnostics utils
|
|– assets/
|   |– fonts/                  # Fonts files
|   |– images/                 # Images
|   |– js/                     # JS libraries
|   |– sass/                   # Sass files
|
|– environment/                # Environment variables
|   |– environment.production        
|   |– environment.qa
|   |– environment.staging
|   |– environment
|
|– local/
|   |– messages.xlf            # Extracted text for localization
– index.html                   # Main file with preloader
– ...
```

## Sass Architecture
The project uses the [7–1 pattern](https://sass-guidelin.es/#architecture), is a common Sass architecture, and is recommended by the Sass Guidelines Project mixed with the concept of Atomic Design.

```bash
sass/
|
|– helpers/
|   |– _variables.scss       # Sass Variables
|   |– _functions.scss       # Sass Functions
|   |– _mixins.scss          # Sass Mixins
|   |– _placeholders.scss    # Sass Placeholders
|   |– helpers.scss          # File importer
|
|– base/
|   |– _reset.scss           # Reset/normalize
|   |– _colors.scss          # Colors variables
|   |– _typography.scss      # Typography rules
|   |– base.scss             # File importer
|
|– components/
|   |– components.scss       # File importer
|   |– atomic-design/        # Atomic design
|       |– atoms/            # Atoms
|       |– molecules/        # Molecules
|       |– organisms/        # Organisms
|       |– templates/        # Templates
|
|– layout/
|   |– _navigation.scss      # Navigation
|   |– _grid.scss            # Grid system
|   |– _header.scss          # Header
|   |– _footer.scss          # Footer
|   |– _sidebar.scss         # Sidebar
|   |– _forms.scss           # Forms
|   |– layout.scss           # File importer
|
|– pages/
|   |– _home.scss            # Home specific styles
|   |– pages.scss            # File importer
|
|– themes/
|   |– _theme.scss           # Default theme
|   |– themes.scss           # File importer 
|
|– vendors/
|   |– _bootstrap.scss       # Bootstrap
|   |– _jquery-ui.scss       # jQuery UI
|   |– vendors.scss          # File importer 
|
– _shame.scss                # Hacks and quick-fixes
– main.scss                  # Main Sass file
```