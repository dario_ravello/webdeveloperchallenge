export const environment = {
  production: true,
  enableAuthenticationToken: false,
  APIEndpoint: 'https://swapi.dev/api',
  APIEndpointMock: 'http://localhost:8000/api'
};
