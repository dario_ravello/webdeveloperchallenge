import * as _ from 'lodash';

/**
    Agnostics utilities for Json
*/
export class JsonUtil {

    /**
     * Returns if the value is a json
     * @param {string} value
     * @returns {boolean}
    */
   static isJson(value: string): boolean {
        try {
            JSON.parse(value);
            return true;
        } catch (e) {
            return false;
        }
    }

    /**
     * Returns the key names from a json
     * @param {any} value
     * @returns {boolean}
    */
    static getKeysNamesFromArray(value: any): string[] {
        try {
            if(this.isJson(JSON.stringify(value))) {
                return _.keys(value[0]);                
            }
            return [];
        } catch (e) {
            return [];
        }
    }
    
}