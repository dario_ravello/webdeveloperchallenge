import * as _ from 'lodash';
import  * as moment from 'moment';

/**
    Agnostics utilities for table component
*/
export class TableUtil {

    /**
     * Returns if a string value is numeric
     * @param {string} value
     * @returns {boolean}
    */
    static isNumber(value: string): Boolean {
        try {
            if(!_.isEmpty(value)){
                return !isNaN(+value);
            }
            return false;
        } catch (e) {
            return false;
        }
    }
    
    /**
     * Returns if a string value is a date
     * @param {string} value
     * @returns {boolean}
    */
    static isDate(value: string): Boolean {
        var formats = [
            "YYYY-MM-DDTHH:mm:ss.SSSSZ"
        ];
        return moment(value, formats, true).isValid();
    }

    /**
     * Returns if a string value is string and check if is not another type
     * @param {string} value
     * @returns {boolean}
    */
    static isString(value: string): Boolean {

        if(this.isDate(value) || this.isNumber(value) || this.isArray(value) || this.isLinkUrl(value) || value === 'unknown') {
            return false;
        }
        return _.isString(value);
    }

    /**
     * Returns if a string value is an array
     * @param {string} value
     * @returns {boolean}
    */
    static isArray(value: string): Boolean {
        return _.isArray(value);
    }

    /**
     * Returns if a string value is a link
     * @param {string} value
     * @returns {boolean}
    */
   static isLinkUrl(value: string): Boolean {
        if(this.isArray(value)) {
            return false;
        }
        var pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
        '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
        '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
        '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
        '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
        '(\\#[-a-z\\d_]*)?$','i'); // fragment locator

    return !!pattern.test(value);
    }
}