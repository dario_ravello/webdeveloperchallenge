import { Component, Input, OnInit, ViewChildren, QueryList } from '@angular/core';
import { SortEvent, NgbdSortableHeader, SortColumn } from '../../directives/sortable.directive';
import { TableUtil } from '../../utils/TableUtil';
import { JsonUtil } from '../../utils/JsonUtil';
import { ApiHttpService } from 'src/app/core/services/api-http.service';
import { ApiEndpointsService } from 'src/app/core/services/api-endpoints.service';
import { QueryStringParameters } from '../../classes/query-string-parameters';
import * as _ from 'lodash';
import { NgxSpinnerService } from 'ngx-spinner';

  /**
  * // TODO: Refactor this component
  * - [ ] Refactor this component abstracting the methods and makeing it reusables... Sorry!
  * - [ ] Adding testing units for some methods 
  * - [ ] Fix the number sorting 
  */

@Component({
  selector: 'qu-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
})
export class TableComponent implements OnInit{
  TableUtil = TableUtil;
  JsonUtil = JsonUtil;
  
  @ViewChildren(NgbdSortableHeader) headers: QueryList<NgbdSortableHeader>;
  @Input() response: any;
  @Input() pageEndPoint: string;

  itemsPerPage: number = 10;
  totalItems: number;
  totalPages: number;
  page: number;
  previousPageUrl: string;
  nextPageUrl: string;
  pages: number[] = [];

  constructor(
    private apiHttpService: ApiHttpService,
    private apiEndpointsService: ApiEndpointsService,
    private spinner: NgxSpinnerService    
  ) {
    this.spinner.show();
  }

  ngOnInit() {
    this.loadPage();
  }

  getTableTitles() {
    let titlesTable = [];

    if (JsonUtil.isJson(JSON.stringify(this.response?.results))) {
      titlesTable = JsonUtil.getKeysNamesFromArray(this.response?.results);
    }
  
    return titlesTable;
  }

  asIsOrder(a, b) {
    return 1;
  }

  onSort({ column, direction }: SortEvent) {
    this.headers.forEach(header => {
      if (header.sortable !== column) {
        header.direction = '';
      }
    });
    
    this.response.results = _.sortBy(this.response.results, [column], [direction]);
  }

  compare(v1: string | number, v2: string | number){
    return v1 < v2 ? -1 : v1 > v2 ? 1 : 0;
  }

  sort(countries: any, column: SortColumn, direction: string): any[] {
    if (direction === '' || column === '') {
      return countries;
    } else {
      return [...countries].sort((a, b) => {
        const res = this.compare(a[column], b[column]);
        return direction === 'asc' ? res : -res;
      });
    }
  }

  loadPage() {
    if (JsonUtil.isJson(JSON.stringify(this.response?.results))) { 
      this.totalItems = this.response.count;
      this.totalPages = this.totalItems / this.itemsPerPage;

      if (!this.response.previous) {
        this.page = 1;
      } else if (!this.response.next) {
        this.page = this.totalItems / this.itemsPerPage;
      } else {
        this.page = this.response.next.split('?page=')[1]-1;
      } 

      this.pages = [];

      for (let index = 0; index < this.totalPages; index++) {
        this.pages.push(index);
      }
      this.spinner.hide();
    }
  }
 
  firstPage() {
    this.spinner.show();
    this.response = null;

    this.response = this.apiHttpService.get(this.apiEndpointsService.createUrl(this.pageEndPoint)).subscribe(data => {
      this.response = data;
      this.loadPage();
    }); 
  }

  previousPage() { 
    this.spinner.show();
    this.response = null;

    this.apiHttpService.get(this.apiEndpointsService.createUrlWithQueryParameters(this.pageEndPoint, 
      (qs: QueryStringParameters) => {
        qs.push('page', this.page - 1);
      } ))
    .subscribe(data => {
        this.response = data;
        this.loadPage();
      }
    );
  }

  openPage(pageSelected) {
    this.spinner.show();
    this.response = null;

    this.apiHttpService.get(this.apiEndpointsService.createUrlWithQueryParameters(this.pageEndPoint, 
      (qs: QueryStringParameters) => {
        qs.push('page', pageSelected);
      } ))
    .subscribe(data => {
        this.response = data;
        this.loadPage();
      }
    );
  }

  nextPage() { 
    this.spinner.show();
    this.response = null;

    this.apiHttpService.get(this.apiEndpointsService.createUrlWithQueryParameters(this.pageEndPoint, 
      (qs: QueryStringParameters) => {
        qs.push('page', this.page + 1);
      } ))
    .subscribe(data => {
        this.response = data;
        this.loadPage();
      }
    );
  }

  lastPage() {
    this.spinner.show();
    this.response = null;
    
    this.apiHttpService.get(this.apiEndpointsService.createUrlWithQueryParameters(this.pageEndPoint, 
      (qs: QueryStringParameters) => {
        qs.push('page', this.totalPages);
      } ))
    .subscribe(data => {
        this.response = data;
        this.loadPage();
      }
    );
   }

  isDisabledFirstButton(): Boolean {
    return this.page===1;
  }

  isDisabledPreviousButton(): Boolean {
    return this.page===1;
  }
  
  isDisabledButton(currentPage): Boolean {
    return this.page===currentPage;
  }
  
  isDisabledNextButton(): Boolean {
    return this.page===this.totalPages;
  }
  
  isDisabledLastButton(): Boolean {
    return this.page===this.totalPages;
  }
}
