import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableComponent } from './table.component'; 
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('TableComponent', () => {
  let component: TableComponent;
  let fixture: ComponentFixture<TableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ],
      declarations: [ TableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
 
  it(`should have pagination first`, () => { 
    fixture.detectChanges(); 
    var compiled = fixture.debugElement.nativeElement;
    var element = compiled.querySelector('#button-pagination-first');
    expect(element.innerHTML).toContain('First'); 
  });
  
  it(`should have pagination previous`, () => { 
    fixture.detectChanges(); 
    var compiled = fixture.debugElement.nativeElement;
    var element = compiled.querySelector('#button-pagination-previous');
    expect(element.innerHTML).toContain('Previous');
  });
    
  it(`should have pagination next`, () => { 
    fixture.detectChanges();
    var compiled = fixture.debugElement.nativeElement;
    var element = compiled.querySelector('#button-pagination-next');
    expect(element.innerHTML).toContain('Next');
  });

  
  it(`should have pagination last`, () => { 
    fixture.detectChanges(); 
    var compiled = fixture.debugElement.nativeElement;
    var element = compiled.querySelector('#button-pagination-last');
    expect(element.innerHTML).toContain('Last'); 
  });
  
  it(`should have pagination one`, () => { 
    var element;
    fixture.detectChanges(); 
    fixture.whenStable().then(() => {
      var compiled = fixture.debugElement.nativeElement;
      element = compiled.querySelector('#button-pagination-1');
      expect(element.innerHTML).toContain('1');
    });
  });
});
