import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'tabletitle'})
export class TableTitlePipe implements PipeTransform {
  
    transform(value: string): string {
        let newString = "";

        if(value.length > 0) {
            newString = value.substr(0,1).toUpperCase() + value.substr(1);
            newString = newString.replace("_", " ");
        }
        
        return newString;
    }
}