import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common'; 
import { ButtonComponent } from './components/button/button.component'; 
import { TableComponent } from './components/table/table.component'; 
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';
import { NgbdSortableHeader } from './directives/sortable.directive';
import { TitleComponent } from './components/title/title.component';
import { TableTitlePipe } from './pipes/table-title.pipe';
import { NgxSpinnerModule } from "ngx-spinner";
 
@NgModule({
  declarations: [
    ButtonComponent, 
    TableComponent,
    TitleComponent,
    TableTitlePipe,
    
    NgbdSortableHeader
  ],
  imports: [
    CommonModule,
    NgbModule,
    NgxSpinnerModule
  ], 
  exports: [
    ButtonComponent, 
    TableComponent,
    NgbdSortableHeader,
    TitleComponent,
    TableTitlePipe,
    
    NgbModule,
    NgxSpinnerModule,
    RouterModule
  ]
})
export class SharedModule { }
