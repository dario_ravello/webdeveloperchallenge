import { SortColumn, SortDirection } from "../directives/sortable.directive";

interface State {
    page: number;
    pageSize: number;
    searchTerm: string;
    sortColumn: SortColumn;
    sortDirection: SortDirection;
  }
  