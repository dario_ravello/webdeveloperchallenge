import { Planet } from "./planet.model";

interface SearchResult {
    countries: Planet[];
    total: number;
}