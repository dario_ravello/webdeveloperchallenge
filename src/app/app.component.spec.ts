import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { Renderer2 } from '@angular/core';
import { browser, by, By, element } from 'protractor';
import { protractor } from 'protractor/built/ptor';

describe('AppComponent', () => {
  let renderer2: Renderer2;
  
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [ Renderer2 ],
      imports: [
        RouterTestingModule
      ],
      declarations: [
        AppComponent
      ]
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'QU - Web Developer Challenge'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app.title).toEqual('QU - Web Developer Challenge');
  });

});
