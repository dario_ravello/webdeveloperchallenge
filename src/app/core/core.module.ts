import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlanetService } from './services/planet.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http'; 
import { ApiHttpService } from './services/api-http.service';
import { ErrorService } from './services/error.service';
import { HttpErrorInterceptor } from './interceptors/http-error.interceptor';
import { AuthenticationInterceptor } from './interceptors/authentication.interceptor';
import { SharedModule } from '../shared/shared.module'; 
import { FooterComponent } from './layout/desktop/footer/footer.component';
import { HeaderComponent } from './layout/desktop/header/header.component';
import { NotFoundComponent } from './layout/desktop/not-found/not-found.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

@NgModule({
  declarations: [
    FooterComponent, 
    HeaderComponent,
    NotFoundComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    SharedModule,
    FontAwesomeModule
  ],
  providers:[
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthenticationInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpErrorInterceptor,
      multi: true
    },
    PlanetService,
    ApiHttpService,
    ErrorService
  ],
  exports: [
    FooterComponent, 
    HeaderComponent,
    NotFoundComponent
  ]
})
export class CoreModule { }
