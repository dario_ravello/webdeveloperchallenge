import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FooterComponent } from './footer.component';

describe('FooterComponent', () => {
  let component: FooterComponent;
  let fixture: ComponentFixture<FooterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FooterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  
  it(`should have the signature`, () => { 
    fixture.detectChanges(); 
    var compiled = fixture.debugElement.nativeElement; 
    var element = compiled.querySelector('#footer-signature');
    expect(element.innerHTML).toContain('by Darío Ravello - Fullstack Developer'); 
  });
});
