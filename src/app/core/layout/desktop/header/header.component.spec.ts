import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderComponent } from './header.component';
import { RouterModule } from '@angular/router';

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ 
        RouterModule.forRoot([]),
      ],
      declarations: [ HeaderComponent ] 
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it(`should have the q logo`, () => { 
    fixture.detectChanges(); 
    var compiled = fixture.debugElement.nativeElement; 
    var element = compiled.querySelector('#header-logo-q');
    expect(element.innerHTML).toContain('svg'); 
  });
  
  it(`should have the u logo`, () => { 
    fixture.detectChanges(); 
    var compiled = fixture.debugElement.nativeElement; 
    var element = compiled.querySelector('#header-logo-u');
    expect(element.innerHTML).toContain('svg'); 
  });
  
  it(`should have home menu`, () => { 
    fixture.detectChanges(); 
    var compiled = fixture.debugElement.nativeElement; 
    var element = compiled.querySelector('#menu-home');
    expect(element.innerHTML).toContain('Home'); 
  });
});
