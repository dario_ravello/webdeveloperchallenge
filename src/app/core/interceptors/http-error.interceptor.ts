import { Injectable } from '@angular/core';
import { HttpHandler, HttpRequest, HttpInterceptor } from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/internal/operators'; 
import { ErrorService } from '../services/error.service';

@Injectable({
  providedIn: 'root',
})
export class HttpErrorInterceptor implements HttpInterceptor {
  errorService: ErrorService; 

  construct(errorService: ErrorService ) { 
    this.errorService = errorService;
  }

  intercept(req: HttpRequest<any>, next: HttpHandler) {

    return next.handle(req).pipe( 
        catchError((error) => {
            let errorMessage = '';
            if (error instanceof ErrorEvent) { 
              errorMessage = `Client-side error: ${error.error.message}`;
            } else { 
              errorMessage = `Server-side error: ${error.status} ${error.message}`;
            }
 
            this.errorService.showError(errorMessage);
            return throwError(errorMessage);
        })
    );
  }
}
