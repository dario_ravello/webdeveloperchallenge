import { Injectable } from '@angular/core';
import { ToastService } from './toast.service';

@Injectable()
export class ErrorService {
    public isDialogOpen: Boolean = false;
    
    constructor(public toastService: ToastService) { }

    showError(data): any {
        if (this.isDialogOpen) {
            return false;
        }
        this.isDialogOpen = true;
        this.toastService.show(data);
    }
}