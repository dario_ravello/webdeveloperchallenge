import { Injectable } from '@angular/core';
import { UrlBuilder } from '../../shared/classes/url-builder';
import { QueryStringParameters } from '../../shared/classes/query-string-parameters';
import { Constants } from '../../config/constants';

@Injectable({
  providedIn: 'root'
})
export class ApiEndpointsService {

  constructor( ) { }
  
  public createUrl( action: string, isMockAPI?: boolean): string {
    const urlBuilder: UrlBuilder = new UrlBuilder( isMockAPI ? Constants.API_MOCK_ENDPOINT : Constants.API_ENDPOINT, action );
    return urlBuilder.toString();
  }

  public createUrlWithQueryParameters( action: string, queryStringHandler?: (queryStringParameters: QueryStringParameters) => void , isMockAPI?: boolean): string {
    const urlBuilder: UrlBuilder = new UrlBuilder( isMockAPI ? Constants.API_MOCK_ENDPOINT : Constants.API_ENDPOINT, action );

    if (queryStringHandler) {
      queryStringHandler(urlBuilder.queryString);
    }

    return urlBuilder.toString();
  }

  public createUrlWithPathVariables( action: string, pathVariables: any[] = [] , isMockAPI?: boolean): string {
    let encodedPathVariablesUrl: string = '';
    
    for (const pathVariable of pathVariables) {
      if (pathVariable !== null) {
        encodedPathVariablesUrl += `/${encodeURIComponent(pathVariable.toString())}`;
      }
    }
    
    const urlBuilder: UrlBuilder = new UrlBuilder( isMockAPI ? Constants.API_MOCK_ENDPOINT : Constants.API_ENDPOINT, `${action}${encodedPathVariablesUrl}` );

    return urlBuilder.toString();
  }
}
