import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IResponse } from '../models/response.model';
import { Planet } from 'src/app/shared/models/planet.model';

@Injectable({
  providedIn: 'root'
})
export class ApiHttpService {
  constructor( private http: HttpClient ) { }

  public get(url: string, options?: any) {
    return this.http.get<IResponse<Planet>>(url, options);
  }

  public post(url: string, data: any, options?: any) {
    return this.http.post(url, data, options);
  }

  public put(url: string, data: any, options?: any) {
    return this.http.put(url, data, options);
  }
  
  public delete(url: string, options?: any) {
    return this.http.delete(url, options);
  }
}