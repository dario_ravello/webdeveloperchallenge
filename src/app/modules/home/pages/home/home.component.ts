import { Component, OnInit } from '@angular/core';
import { ApiHttpService } from 'src/app/core/services/api-http.service';
import { ApiEndpointsService } from 'src/app/core/services/api-endpoints.service';
import { Constants } from 'src/app/config/constants';
import { IResponse } from 'src/app/core/models/response.model';
import { Planet } from 'src/app/shared/models/planet.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  planetList: IResponse<Planet>;
  apiEndPoint: string;

  constructor( private apiHttpService: ApiHttpService, private apiEndpointsService: ApiEndpointsService ) {  }

  ngOnInit() { 
    this.apiEndPoint = Constants.API_PLANETS_ENDPOINT;
    this.apiHttpService.get(this.apiEndpointsService.createUrl(this.apiEndPoint))
    .subscribe(data => {
        this.planetList = data as unknown as IResponse<Planet>;
      }
    );
  }
}
