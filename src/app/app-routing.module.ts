import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router'; 
import { NotFoundComponent } from './core/layout/desktop/not-found/not-found.component';


const routes: Routes = [
  {
   path: '', 
   loadChildren: () => import('./modules/home/home.module').then(m => m.HomeModule) 
  },
  {
    path: 'home', 
    loadChildren: () => import('./modules/home/home.module').then(m => m.HomeModule) 
   },
  { path: '404', component: NotFoundComponent },
  { path: '**', redirectTo: '/404' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
