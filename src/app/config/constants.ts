import { Injectable } from '@angular/core';
import { environment } from './../../environments/environment';

@Injectable()
export class Constants {
    public static readonly API_ENDPOINT: string = environment.APIEndpoint;
    public static readonly API_MOCK_ENDPOINT: string = environment.APIEndpointMock;
    public static readonly API_PLANETS_ENDPOINT: string = 'planets/';
}